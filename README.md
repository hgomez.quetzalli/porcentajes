# Calculadora de Porcentajes - _App Android_

Esta aplicación Android ofrece una variedad de funciones útiles relacionadas con porcentajes para facilitar tus cálculos rápidos. A continuación, se describen las características clave:

## 1. Calcular el porcentaje de una cantidad dada

Ingresa una cantidad y un porcentaje, y la aplicación calculará automáticamente el resultado.

## 2. Calcular el total conociendo un porcentaje

Determina el total al conocer una cantidad y un porcentaje adicional.

## 3. Calcular el porcentaje del total de una cantidad

Obtén el porcentaje correspondiente a una cantidad en relación con un total conocido.

## 4. Calcular el precio final tras aplicar un porcentaje de descuento

Calcula el precio final después de aplicar un porcentaje de descuento.

## 5. Calcular el precio final tras aplicar un porcentaje de aumento

Determina el precio final después de aplicar un porcentaje de aumento.

## 6. Calcular el porcentaje aplicado al precio inicial conociendo el precio final

Descubre el porcentaje aplicado al precio inicial al conocer tanto el precio inicial como el precio final. Si el porcentaje es negativo, se trata de un descuento.

¡Con esta aplicación, realizar cálculos con porcentajes es más fácil que nunca! Personaliza los ejemplos según la interfaz y la lógica específica de tu aplicación. ¡Espero que sea de gran utilidad!
