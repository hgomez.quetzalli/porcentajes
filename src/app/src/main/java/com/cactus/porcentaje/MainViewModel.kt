package com.cactus.porcentaje

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val mainRepository: MainTasks) : ViewModel(){

    private val _calculo1 = MutableLiveData<Double>()
    val calculo1 : LiveData<Double?>
        get() = _calculo1

    private val _calculo2 = MutableLiveData<Double>()
    val calculo2 : LiveData<Double?>
        get() = _calculo2

    private val _calculo3 = MutableLiveData<Double>()
    val calculo3 : LiveData<Double?>
        get() = _calculo3

    private val _calculo4 = MutableLiveData<Double>()
    val calculo4 : LiveData<Double?>
        get() = _calculo4

    private val _calculo5 = MutableLiveData<Double>()
    val calculo5 : LiveData<Double?>
        get() = _calculo5

    private val _calculo6 = MutableLiveData<Double>()
    val calculo6 : LiveData<Double?>
        get() = _calculo6

    fun getCalculo1(porcentaje:Double, cantidad:Double){
        val mainRepository = MainRepository()
        _calculo1.value = mainRepository.getCalculo1(porcentaje, cantidad)
    }

    fun getCalculo2(porcentaje:Double, cantidad:Double){
        val mainRepository = MainRepository()
        _calculo2.value = mainRepository.getCalculo2(porcentaje, cantidad)
    }

    fun getCalculo3(cantidad1:Double, cantidad2:Double){
        val mainRepository = MainRepository()
        _calculo3.value = mainRepository.getCalculo3(cantidad1, cantidad2)
    }

    fun getCalculo4(cantidad:Double, porcentaje:Double){
        val mainRepository = MainRepository()
        _calculo4.value = mainRepository.getCalculo4(cantidad, porcentaje)
    }

    fun getCalculo5(cantidad:Double, porcentaje:Double){
        val mainRepository = MainRepository()
        _calculo5.value = mainRepository.getCalculo5(cantidad, porcentaje)
    }

    fun getCalculo6(cantidad1:Double, cantidad2:Double){
        val mainRepository = MainRepository()
        _calculo6.value = mainRepository.getCalculo6(cantidad1, cantidad2)
    }

}