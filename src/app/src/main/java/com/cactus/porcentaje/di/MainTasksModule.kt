package com.cactus.porcentaje.di

import com.cactus.porcentaje.MainRepository
import com.cactus.porcentaje.MainTasks
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class MainTasksModule {
    @Binds
    abstract fun bindMainTasks(mainRepository: MainRepository): MainTasks
}