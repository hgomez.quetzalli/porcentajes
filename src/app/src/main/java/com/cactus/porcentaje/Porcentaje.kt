package com.cactus.porcentaje

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class Porcentaje: Application()