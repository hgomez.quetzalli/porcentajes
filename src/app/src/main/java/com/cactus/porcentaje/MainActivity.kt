package com.cactus.porcentaje

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.cactus.porcentaje.databinding.ActivityMainBinding
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.android.material.carousel.CarouselSnapHelper
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    lateinit var mAdView : AdView
    private lateinit var binding: ActivityMainBinding
    private val mainViewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        MobileAds.initialize(this) {}
        mAdView = findViewById(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)

        binding.idBtnCalc1.setOnClickListener {
            try {
                val porcentaje:Double = binding.idText1Calc1.text.toString().toDouble()
                val cantidad:Double = binding.idText2Calc1.text.toString().toDouble()
                mainViewModel.getCalculo1(porcentaje, cantidad)
            }catch (e: Exception){
                Toast.makeText(this, R.string.campo_vacio, Toast.LENGTH_SHORT).show()
            }
        }
        mainViewModel.calculo1.observe(this){
            result ->
            binding.idTextViewResultCalc1.text = result.toString()
        }

        hideHint(binding.idText1Calc1, "20")
        hideHint(binding.idText2Calc1, "100")

        binding.idBtnCalc2.setOnClickListener {
            try {
                val porcentaje:Double = binding.idText1Calc2.text.toString().toDouble()
                val cantidad:Double = binding.idText2Calc2.text.toString().toDouble()
                mainViewModel.getCalculo2(porcentaje, cantidad)
            }catch (e: Exception){
                Toast.makeText(this, R.string.campo_vacio, Toast.LENGTH_SHORT).show()
            }
        }

        mainViewModel.calculo2.observe(this){
                result ->
            binding.idTextViewResultCalc2.text = result.toString()
        }

        hideHint(binding.idText1Calc2, "10")
        hideHint(binding.idText2Calc2, "20")

        binding.idBtnCalc3.setOnClickListener {
            try {
                val cantidad1:Double = binding.idText1Calc3.text.toString().toDouble()
                val cantidad2:Double = binding.idText2Calc3.text.toString().toDouble()
                mainViewModel.getCalculo3(cantidad1, cantidad2)
            }catch (e: Exception){
                Toast.makeText(this, R.string.campo_vacio, Toast.LENGTH_SHORT).show()
            }
        }

        mainViewModel.calculo3.observe(this){
                result ->
            binding.idTextViewResultCalc3.text = result.toString() + "%"
        }

        hideHint(binding.idText1Calc3, "200")
        hideHint(binding.idText2Calc3, "40")

        binding.idBtnCalc4.setOnClickListener {
            try {
                val cantidad1:Double = binding.idText1Calc4.text.toString().toDouble()
                val cantidad2:Double = binding.idText2Calc4.text.toString().toDouble()
                mainViewModel.getCalculo4(cantidad1, cantidad2)
            }catch (e: Exception){
                Toast.makeText(this, R.string.campo_vacio, Toast.LENGTH_SHORT).show()
            }
        }

        mainViewModel.calculo4.observe(this){
                result ->
            binding.idTextViewResultCalc4.text = result.toString()
        }

        hideHint(binding.idText1Calc4, "60")
        hideHint(binding.idText2Calc4, "10")

        binding.idBtnCalc5.setOnClickListener {
            try {
                val cantidad1:Double = binding.idText1Calc5.text.toString().toDouble()
                val cantidad2:Double = binding.idText2Calc5.text.toString().toDouble()
                mainViewModel.getCalculo5(cantidad1, cantidad2)
            }catch (e: Exception){
                Toast.makeText(this, R.string.campo_vacio, Toast.LENGTH_SHORT).show()
            }
        }

        mainViewModel.calculo5.observe(this){
                result ->
            binding.idTextViewResultCalc5.text = result.toString()
        }

        hideHint(binding.idText1Calc5, "60")
        hideHint(binding.idText2Calc5, "10")

        binding.idBtnCalc6.setOnClickListener {

            try {
                val cantidad1:Double = binding.idText1Calc6.text.toString().toDouble()
                val cantidad2:Double = binding.idText2Calc6.text.toString().toDouble()
                mainViewModel.getCalculo6(cantidad1, cantidad2)
            }catch (e: Exception){
                Toast.makeText(this, "Campos vacios", Toast.LENGTH_SHORT).show()
            }
        }

        mainViewModel.calculo6.observe(this){
                result ->
            binding.idTextViewResultCalc6.text = result.toString() + "%"
        }

        hideHint(binding.idText1Calc6, "10")
        hideHint(binding.idText2Calc6, "5")
        
    }

    fun hideHint(editText: EditText, hint:String){
        editText.setOnFocusChangeListener(View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                editText.setHint("");
            } else {
                editText.setHint(hint);
            }
        })
    }


}