package com.cactus.porcentaje

import javax.inject.Inject

interface MainTasks {

    fun getCalculo1(porcentaje:Double, cantidad:Double): Double
    fun getCalculo2(porcentaje:Double, cantidad:Double): Double
    fun getCalculo3(cantidad1:Double, cantidad2:Double): Double
    fun getCalculo4(cantidad:Double, porcentaje:Double): Double
    fun getCalculo5(cantidad:Double, porcentaje:Double): Double
    fun getCalculo6(cantidad1:Double, cantidad2:Double): Double
}

class MainRepository @Inject constructor() : MainTasks{
    override fun getCalculo1(porcentaje: Double, cantidad: Double): Double {
        return (porcentaje * cantidad)/100
    }

    override fun getCalculo2(porcentaje: Double, cantidad: Double): Double {
        return (cantidad*100)/porcentaje
    }

    override fun getCalculo3(cantidad1: Double, cantidad2: Double): Double {
        return (cantidad2 * 100)/cantidad1
    }

    override fun getCalculo4(cantidad: Double, porcentaje: Double): Double {
        return (cantidad*(100 - porcentaje))/100
    }

    override fun getCalculo5(cantidad: Double, porcentaje: Double): Double {
        return (cantidad * (100 + porcentaje)) / 100
    }

    override fun getCalculo6(cantidad1: Double, cantidad2: Double): Double {
        return (cantidad2 * 100) / cantidad1
    }
}